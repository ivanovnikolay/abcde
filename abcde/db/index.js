﻿var sqlite = require('sqlite3');
var config = require('config');
var logger = require('log4js');
var fs = require('fs');
var path = require('path');

var log = logger.getLogger('db');
var sqlfile = path.join(__dirname, 'init.sql');

log.info('Initialize database...');

var db = new sqlite.Database(config.connectionString, function (err) {
	if (err) throw err;
});

var sql = fs.readFile(sqlfile, 'utf8', function (err, data) {
	if (err) throw err;
	
	db.serialize(function () {
		db.exec(data, function (err) {
			if (err) throw err;
		});
	});
});

module.exports = db;
