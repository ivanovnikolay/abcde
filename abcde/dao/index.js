﻿var db = require('../db');
var logger = require('log4js');

var log = logger.getLogger('dao');

var sqlSelect = "SELECT NODES.ID, NAME, DESCRIPTION, NODES.CREATED_TIME, MODIFIED_TIME FROM NODES ";
var nodeParentsForListSql = sqlSelect + " INNER JOIN PATHS ON NODES.ID = PATHS.PARENT_ID WHERE PATHS.CHILD_ID IN ($id)";
var nodeChildsSql = sqlSelect + " INNER JOIN PATHS ON NODES.ID = PATHS.CHILD_ID WHERE PATHS.PARENT_ID = $id";
var nodeNotLinkedChildsSql = sqlSelect + " LEFT JOIN PATHS ON NODES.ID = PATHS.CHILD_ID WHERE PATHS.PARENT_ID IS NULL OR PATHS.PARENT_ID = 0";
var toNode = function (row) {
	if (!row) {
		return null;
	}
	return {
		id: row.ID,
		parent_id: row.PARENT_ID,
		name: row.NAME,
		desc: row.DESCRIPTION,
		created_time: new Date(row.CREATED_TIME),
		modified_time: new Date(row.MODIFIED_TIME)
	};
}

var arrayObjectIndexOf = function(myArray, searchTerm, property) {
    for (var i = 0, len = myArray.length; i < len; i++) {
        if (myArray[i][property] === searchTerm) return i;
    }
    return -1;
}

exports.getNode = function (id, cb) {
	db.get(sqlSelect + "WHERE ID = $id", { $id: id }, function (err, row) {
		if (cb) {
			cb(err, toNode(row));
		}
	});
}

exports.linkNodes = function (from_id, to_id, cb) {
    db.get("SELECT COUNT() AS CNT FROM PATHS WHERE PARENT_ID = $to_id AND CHILD_ID = $from_id", { $to_id: to_id, $from_id: from_id }, function (err, count) {
        if (err) {
            cb(err);
        } else {
            if (count.CNT == 0) {
                db.run("INSERT INTO PATHS (PARENT_ID, CHILD_ID) VALUES ($to_id, $from_id)", { $to_id: to_id, $from_id: from_id }, function (err) {
                    if (cb) {
                        cb(err);
                    }
                });
            } else {
                cb(null);
            }
        }
    });
}

exports.unlinkNodes = function (from_id, to_id, cb) {
    db.run("DELETE FROM PATHS WHERE PARENT_ID = $to_id AND CHILD_ID = $from_id", { $to_id: to_id, $from_id: from_id }, function (err) {
        if (err) {
            cb(err);
        } else {
            db.get("SELECT COUNT() AS CNT FROM PATHS WHERE CHILD_ID = $from_id", { $from_id: from_id }, function (err, count) {
                if (err) {
                    cb(err);
                } else {
                    if (count.CNT == 0) {
                        db.run("DELETE FROM NODES WHERE ID = $from_id", { $from_id: from_id }, function (err) {
                            if (err) {
                                cb(err);
                            } else {
                                db.run("DELETE FROM PATHS WHERE PARENT_ID = $from_id", { $from_id: from_id }, function (err) {
                                    cb(err);
                                });
                            }
                        });
                    } else {
                        cb(null);
                    }
                }
            });
        }
    });
}

exports.getChilds = function (parent_id, cb) {
    if (parent_id == 0) {
        db.all(nodeNotLinkedChildsSql, function (err, rows) {
            if (cb) {
                cb(err, rows != null ? rows.map(toNode) : null);
            }
        });
    } else {
        db.all(nodeChildsSql, { $id: parent_id || 0 }, function (err, rows) {
            if (cb) {
                cb(err, rows != null ? rows.map(toNode) : null);
            }
        });
    }
}

exports.getParents = function (id, cb) {
    var self = function (parent_ids, nodes) {
        if (!nodes) nodes = [];
        var new_parents = [];
        db.all(nodeParentsForListSql, { $id: parent_ids.join() }, function (err, rows) {
            if (err) return cb(err);
            for (var index in rows) {
                var row = rows[index];
                if (arrayObjectIndexOf(nodes, row.ID, "ID") == -1)
                {
                    nodes.push(row);
                    new_parents.push(row.ID);
                }
            }
            if (new_parents.length > 0) {
                self(new_parents.reverse(), nodes);
            } else {
                if (cb) {
                    cb(null, nodes.map(toNode).reverse());
                }
            }
        });
    }
    self([id]);
}

exports.getTree = function (cb, node_id) {
    db.all("SELECT ID, PARENT_ID, NAME FROM NODES ORDER BY ID", function (err, rows) {
        if (cb) {
            var result = null;
            if (rows) {
                rows = rows.map(function (row) {
                    return { id: row.ID, parent_id: row.PARENT_ID, text: row.NAME, state: { selected: node_id == row.ID } };
                });
                var result = [];
                var map = {};
                for (var i = 0; i < rows.length; i++) {
                    var node = rows[i];
                    map[node.id] = i;
                    if (node.parent_id !== 0) {
                        var mapped = rows[map[node.parent_id]];
                        if (!mapped.nodes) {
                            mapped.nodes = [];
                        }
                        mapped.nodes.push(node);
                    } else {
                        result.push(node);
                    }
                }
            }
            cb(err, result);
        }
    });
}

exports.saveNode = function (node, cb) {
	var sql = "INSERT INTO NODES (NAME, DESCRIPTION) VALUES " +
		"($name, $desc)";
	var params = {
		$name: node.name || null,
		$desc: node.desc || null,
	};
	if (node.id) {
		params.$id = node.id;
		params.$mod_time = new Date();
		sql = "UPDATE NODES SET NAME = $name, DESCRIPTION = $desc, MODIFIED_TIME = $mod_time WHERE ID = $id";
	}
	db.run(sql, params, function (err) {
		if (!err && !node.id) {
			node.id = this.lastID;
		}
		if (cb) {
			cb(err, node);
		}
	});
}

exports.deleteNode = function (id, cb) {
	db.run("DELETE FROM NODES WHERE ID = $id", { $id: id }, function (err) {
		if (cb) {
			cb(err);
		}
	});
}

var toAppeal = function (row) {
    if (!row) {
        return null;
    }
    return {
        id: row.ID,
        node_id: row.NODE_ID,
        client: row.CLIENT,
        question: row.QUESTION,
        new_response: row.NEW_RESPONSE,
        start_time: new Date(row.START_TIME)
    };
}

exports.saveAppeal = function (appeal, cb) {
    var sql = "INSERT INTO APPEALS (NODE_ID, CLIENT, QUESTION, NEW_RESPONSE, START_TIME) VALUES " +
		"($node_id, $client, $question, $new_response, $start_time)";
    var params = {
        $node_id: appeal.node_id || 0,
        $client: appeal.client || null,
        $question: appeal.question || null,
        $new_response: appeal.new_response || null,
        $start_time: appeal.start_time || null,
    };
    db.run(sql, params, function (err) {
        if (!err && !appeal.id) {
            appeal.id = this.lastID;
        }
        if (cb) {
            cb(err, appeal);
        }
    });
}

exports.getAppeals = function (cb, offset, limit) {
    db.all("SELECT ID, NODE_ID, CLIENT, QUESTION, NEW_RESPONSE, START_TIME FROM APPEALS WHERE NEW_RESPONSE IS NOT NULL ORDER BY START_TIME LIMIT $limit OFFSET $offset", { $limit: limit || 100, $offset: offset || 0 }, function (err, rows) {
        if (rows) {
            rows = rows.map(toAppeal);
        }
        if (cb) {
            cb(err, rows);
        }
    });
}

exports.getAppeal = function (id, cb) {
    db.get("SELECT ID, NODE_ID, CLIENT, QUESTION, NEW_RESPONSE, START_TIME FROM APPEALS WHERE ID = $id", { $id: id }, function (err, row) {
        if (cb) {
            cb(err, toAppeal(row));
        }
    });
}

exports.clearAppeal = function (id, cb) {
    db.get("UPDATE APPEALS SET NEW_RESPONSE=NULL WHERE ID = $id", { $id: id }, function (err, row) {
        if (cb) {
            cb(err, toAppeal(row));
        }
    });
}

exports.closeAppeal = function (appeal, cb) {
    var params = {
        $id: appeal.id || 0,
        $node_id: appeal.node_id || 0,
        $new_response: appeal.new_response || null
    };
    
    db.get("UPDATE APPEALS SET NEW_RESPONSE=$new_response, NODE_ID = $node_id WHERE ID = $id", params, function (err, row) {
        if (cb) {
            cb(err);
        }
    });
}
