#!/bin/sh

# install programs
apt-get update
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
apt-get install -y git nodejs build-essential

# download project
git clone https://ivanovnikolay@bitbucket.org/ivanovnikolay/abcde.git /var/www/abcde
cd /var/www/abcde/abcde
npm install
npm install pm2 -g

# setup and run project
pm2 startup ubuntu
chmod +x /etc/init.d/pm2-init.sh && update-rc.d pm2-init.sh defaults
NODE_ENV=production NODE_CONFIG_DIR=/var/www/abcde/abcde/config pm2 start bin/www
pm2 save
