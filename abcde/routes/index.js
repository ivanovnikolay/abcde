﻿var express = require('express');

var dao = require('../dao');
var log4js = require('log4js');

var router = express.Router();
var log = log4js.getLogger("router");

/* GET operator page. */
router.get('/', function (req, res) {
    res.render('index', { menu: 'operator', title: 'Система обработки сообщений клиентов' });
});

/* GET operator appeal page. */
router.get('/appeal', function (req, res) {
    res.render('appeal', { menu: 'operator', title: 'Начало обслуживания' });
});

/* POST operator appeal page. */
router.post('/appeal', function (req, res) {
    var appeal = {
        start_time: req.body.start_time,
        client: req.body.client,
        question: req.body.question,
    };
    dao.saveAppeal(appeal, function (err, appeal) {
        if (err) throw err;
        res.redirect("/browse/" + appeal.id);
    });
});

/* GET operator browse page. */
router.get('/browse/:id', function (req, res, next) {
    var id = req.params.id;
    var node_id = req.query.node_id || 0;
    fillNodePageData(node_id, function (err, data) {
        if (err) return next(err);
        dao.getAppeal(id, function (err, appeal) {
            if (err) return next(err);
            res.render('browse', { menu: 'operator', title: 'Продолжение обслуживания', appeal: appeal, subnodes: data.subnodes, item: data.item, nodes: data.nodes, url: "/browse/" + id + "?node_id=" });
        });
    });
});

/* POST operator browse page. */
router.post('/browse/:id', function (req, res) {
    var appeal = {
        id: req.params.id,
        node_id: req.query.node_id,
    };
    if (req.body.newq) {
        appeal.new_response = req.body.newqtext;
    }
    dao.closeAppeal(appeal, function (err, appeal) {
        if (err) throw err;
        res.redirect("/");
    });
});

/* GET admin page */
router.get('/admin', function (req, res) {
    res.render('admin/index', { menu: 'admin', title: 'Администрирование обращений' });
});

router.get('/admin/basefill', function (req, res, next) {
    res.redirect("/admin/basefill/0");
});

var fillNodePageData = function (id, cb) {
    if (id == 0) {
        dao.getChilds(0, function (err, subnodes) {
            cb(err, { subnodes: subnodes });
        });
    } else {
        dao.getNode(id, function (err, item) {
            if (err) cb(err);
            dao.getParents(item.id, function (err, parents) {
                if (err) return cb(err);
                dao.getChilds(item.id, function (err, subnodes) {
                    cb(err, { item: item, nodes: parents, subnodes: subnodes });
                });
            });
        });
    }
}

router.get('/admin/basefill/:id', function (req, res, next) {
    var id = req.params.id;
    fillNodePageData(id, function (err, data) {
        if (err) {
            return next(data);
        } else {
            res.render('admin/basefill', { menu: 'admin', title: 'Наполнение базы', subnodes: data.subnodes, item: data.item, nodes: data.nodes, editable: true, url: "/admin/basefill/" });
        }
    });
});

router.get('/admin/edit/:id', function (req, res, next) {
    var id = req.params.id;
    dao.getNode(id, function (err, item) {
        if (err) return next(err);
        dao.getParents(item.id, function (err, parents) {
            if (err) return next(err);
            res.render('admin/edit', { menu: 'admin', title: 'Редактирование', id: id, item: item, nodes: parents });
        });
    });
});

router.post('/admin/edit/:id', function (req, res, next) {
    var id = req.params.id;
    var node = {
        id: id, name: req.body.name, desc: req.body.desc
    };
    dao.saveNode(node, function (err, item) {
        if (err) return next(err);
        res.redirect("/admin/basefill/" + id);
    });
});

router.get('/admin/new/:parent_id', function (req, res, next) {
    var id = req.params.parent_id;
    if (id == 0) {
        dao.getParents(id, function (err, parents) {
            if (err) return next(err);
            res.render('admin/edit', { menu: 'admin', title: 'Добавить', id: id, item: { name: "", desc: "" }, nodes: parents });
        });
    } else {
        dao.getNode(id, function (err, item) {
            if (err) return next(err);
            dao.getParents(item.id, function (err, parents) {
                if (err) return next(err);
                parents.push(item);
                res.render('admin/edit', { menu: 'admin', title: 'Добавить', id: id, item: { name: "", desc: "" }, nodes: parents });
            });
        });
    }
});

router.post('/admin/new/:parent_id', function (req, res, next) {
    var id = req.params.parent_id;
    var node = {
        name: req.body.name, desc: req.body.desc
    };
    dao.saveNode(node, function (err, item) {
        if (err) return next(err);
        dao.linkNodes(item.id, id, function (err) {
            if (err) return next(err);
            res.redirect("/admin/basefill/" + item.id);
        });
    });
});

router.get('/admin/unlink/:id/:to_id', function (req, res, next) {
    var id = req.params.id;
    var to_id = req.params.to_id;
    dao.unlinkNodes(id, to_id, function (err) {
        if (err) return next(err);
        res.redirect("/admin/basefill/" + (to_id == 0 ? "" : to_id));
    });
});

router.get('/admin/link/:id/:to_id', function (req, res, next) {
    var id = req.params.id;
    var to_id = req.params.to_id;
    dao.getNode(id, function (err, link_item) {
        if (err) return next(err);
        dao.getNode(to_id, function (err, item) {
            if (err) return next(err);
            dao.getParents(to_id, function (err, parents) {
                if (err) return next(err);
                dao.getChilds(to_id, function (err, subnodes) {
                    if (err) return next(err);
                    res.render('admin/link', { menu: 'admin', title: 'Привязка', link_item: link_item, item: item, nodes: parents, subnodes: subnodes });
                });
            });
        });
    });
});

router.post('/admin/link/:id/:to_id', function (req, res, next) {
    var id = req.params.id;
    var to_id = req.params.to_id;
    if (id == to_id) return next(new Error("Can't link to himself"));
    dao.linkNodes(id, to_id, function(err) {
        if (err) return next(err);
        res.redirect("/admin/basefill/" + id);
    });
});

router.get('/admin/approval', function (req, res) {
    dao.getAppeals(function (err, appeals) {
        if (err) throw err;
        res.render('admin/approval', { menu: 'admin', title: 'Обработка вариантов оператора', appeals: appeals });
    });
});

router.get('/admin/approve/:id', function (req, res, next) {
    var id = req.params.id;
    var node_id = req.query.node_id;
    if (!id) return next(new Error("id is missing"));
    dao.getAppeal(id, function (err, appeal) {
        if (err) throw err;
        if (appeal.new_response == null) return next(new Error("new response = null"));
        fillNodePageData(node_id || appeal.node_id, function (err, data) {
            if (err) throw err;
            res.render('admin/approve', { menu: 'admin', title: 'Обработка нового варианта', appeal: appeal, subnodes: data.subnodes, item: data.item, nodes: data.nodes, url: "/admin/approve/" + id + "?node_id=" });
        }, appeal.node_id);
    });
});

/* POST admin approve page. */
router.post('/admin/approve/:id', function (req, res) {
    var id = req.params.id;
    if (!id) throw new Error("id is missing");
    
    if (req.body.decline) {
        dao.clearAppeal(req.params.id);
        res.redirect("/admin/approval");
        return;
    }
    var node = {
        name: req.body.name,
        desc: req.body.desc,
        parent_id: req.query.node_id || req.body.node_id
    };
    
    dao.getAppeal(req.params.id, function (err, appeal) {
        if (err) throw err;
        if (appeal.new_response == null) throw new Error("new response = null");
        dao.saveNode(node, function (err, new_node) {
            if (err) throw err;
            dao.linkNodes(new_node.id, node.parent_id || appeal.node_id);
            dao.clearAppeal(req.params.id);
            res.redirect("/admin/approval");
        });
    });
});

/* API methods */
router.get('/api/v1/tree', function (req, res) {
    dao.getTree(function (err, tree) {
        if (err) throw err;
        res.json(tree);
    });
});

router.get('/api/v1/node/:id', function (req, res) {
    var id = req.params.id;
    if (!id) {
        res.sendStatus(400);
        return;
    }
    dao.getNode(id, function (err, node) {
        if (err) throw err;
        res.json(node);
    });
});

router.get('/api/v1/appeals', function (req, res) {
    dao.getAppeals(function (err, appeals) {
        if (err) throw err;
        res.json(appeals);
    }, req.params.offset, req.params.limit);
});


module.exports = router;
