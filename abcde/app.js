﻿var path = require('path');
var express = require('express');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var log4js = require('log4js');

var log = log4js.getLogger("app");
var routes = require('./routes/index');
var app = express();

var publicpath = path.join(__dirname, 'public');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(publicpath, 'favicon.ico'));
app.use(log4js.connectLogger(log4js.getLogger("http"), { level: 'auto' }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(require('stylus').middleware(publicpath));
app.use(express.static(publicpath));

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
	app.use(function (err, req, res, next) {
		log.error("express error handler: ", err);
		res.status(err.status || 500);
        res.render('error', {
            title: 'Ошибка',
			message: err.message,
			error: err
		});
	});
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
	log.error("express error handler: ", err);
	res.status(err.status || 500);
    res.render('error', {
        title: 'Ошибка',
		message: err.message,
		error: {}
	});
});


module.exports = app;
